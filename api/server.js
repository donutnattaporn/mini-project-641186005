const express = require('express') // ใช้งาน module express
const app = express() // สร้างตัวแปร app เป็น instance ของ express
const config = require('config') // เรียกใช้งานและกำหนด Instance ของโมดูล config
const PORT = config.get('port')
app.use(express.json())

const authenRoute = require('./route/Authen')
const memberRoute = require('./route/Member')
app.use('/authen', authenRoute)
app.use('/member', memberRoute)
app.get('/', (req, res) => {
    res.json({
        success: true
    })
})
app.listen(PORT, function() {
    console.log(`Server is runnung on port ${PORT}!`)
})